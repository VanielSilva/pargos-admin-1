import * as React from "react";
import {
	Toolbar,
	SaveButton,
	// DeleteButton,
} from 'react-admin';

const CustomToolbar = props => (
	<Toolbar {...props}>
		<SaveButton />
		{/* <DeleteButton mutationMode="pessimistic" /> */}
	</Toolbar>
);

export default CustomToolbar;