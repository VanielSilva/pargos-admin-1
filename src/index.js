import React from 'react';
import { render } from 'react-dom';
import { Admin, Resource } from 'react-admin';

import './index.css';
import reportWebVitals from './reportWebVitals';

import i18nProvider from './providers/i18n';
import authProvider from './providers/auth';
import dataProvider from './providers/data';

import SaleResource from './resources/sale';
import UserResource from './resources/user';
import PlanResource from './resources/plan';

render(
	<Admin
		title="Pargos"
		i18nProvider={i18nProvider}
		authProvider={authProvider}
		dataProvider={dataProvider}>
		{role => [
			<Resource {...SaleResource} />,
			role === 'root'
				? <Resource {...UserResource} />
				: null,
			role === 'root'
				? <Resource {...PlanResource} />
				: null,
		]}
	</Admin>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();