import * as React from 'react';
import { List, Datagrid, Edit, Create, TextField, NumberField, EditButton, NumberInput, TextInput, required, SimpleForm } from 'react-admin';
import Icon from '@material-ui/icons/LocalOffer';

import Toolbar from '../components/toolbar';

const ViewTitle = ({ record }) => {
	return <span>Plano {record ? `#${record.id}` : ''}</span>;
};

const resource = {
	name: 'plan',
	options: {
		label: 'Planos',
	},
	icon: Icon,
	list: (props) => (
		<List {...props} bulkActionButtons={false}>
			<Datagrid>
				<TextField label='#' source='id' sortable={false} />
				<TextField label='Nome' source='name' sortable={false} />
				<NumberField label='Valor' source='value' sortable={false} options={{ style: 'currency', currency: 'BRL' }} />
				<NumberField label='Comissão' source='comission' sortable={false} options={{ style: 'percent' }} />
				<EditButton basePath='/plan' />
			</Datagrid>
		</List>
	),
	edit: (props) => (
		<Edit title={<ViewTitle />} {...props}>
			<SimpleForm toolbar={<Toolbar />}>
				<TextInput label='Nome' source='name' validate={[required()]} />
				<NumberInput label='Valor' source='value' validate={[required()]} format={v => v.toFixed(2)} parse={v => parseFloat(v)} />
				<NumberInput label='Comissão (%)' source='comission' validate={[required()]} format={v => (v * 100.0).toFixed(2)} parse={v => parseFloat(v) / 100.0} />
				<TextInput label='Info' source='info' />
			</SimpleForm>
		</Edit>
	),
	create: (props) => (
		<Create title='Criar plano' {...props}>
			<SimpleForm redirect='list'>
				<TextInput label='Nome' source='name' validate={[required()]} />
				<NumberInput label='Valor' source='value' initialValue='0' validate={[required()]} format={v => v.toFixed(2)} parse={v => parseFloat(v)} />
				<NumberInput label='Comissão (%)' source='comission' initialValue='0' validate={[required()]} format={v => (v * 100.0).toFixed(2)} parse={v => parseFloat(v) / 100.0} />
				<TextInput label='Info' source='info' />
			</SimpleForm>
		</Create>
	),
};

export default resource;
